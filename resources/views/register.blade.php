<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>JCC Pertemuan 1 | Form</title>
</head>

<body>
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>

    <form action="/welcome" method="post">
        @csrf
        <label for="firstName">First Name</label><br /><br />
        <input type="text" name="firstName" id="firstName" required />
        <br /><br />

        <label for="lastName">Last Name</label> <br /><br />
        <input type="text" name="lastName" id="lastName" required />
        <br /><br />

        <label for="gender">Gender</label> <br /><br />
        <input type="radio" name="gender" id="gender" value="male" /> Male <br />
        <input type="radio" name="gender" id="gender" value="female" /> Female <br /><br />

        <label for="nationality">Nationality</label> <br /><br />
        <select name="nationality" id="nationality" required>
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
            <option value="Zimbabwe">Zimbabwe</option>
        </select>
        <br /><br />

        <label for="lang">Language Spoken</label><br /><br />
        <input type="checkbox" name="lang" id="lang" value="Indonesia" />
        Indonesia <br />
        <input type="checkbox" name="lang" id="lang" value="English" /> English
        <br />
        <input type="checkbox" name="lang" id="lang" value="Other" /> Other
        <br /><br />
        <br />

        <label for="bio">Bio</label> <br /><br />
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea>
        <br /><br />

        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>